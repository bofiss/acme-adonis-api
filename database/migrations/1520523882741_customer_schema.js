'use strict'

const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.string('name')
      table.string('description')
      table.integer('user_id').unsigned()
      table.foreign('user_id')
        .references('users.id')
        .onDelete('cascade')
      table.timestamps()
    
    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomerSchema
