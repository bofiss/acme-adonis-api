'use strict'

const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.table('projects', (table) => {
      table.boolean('completed')
    })
  }

  down () {
    this.table('projects', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectSchema
