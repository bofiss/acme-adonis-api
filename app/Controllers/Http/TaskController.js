'use strict'

class TaskController {
  async index ({request, response}) {
    const tasks = await Task.all();
    response.status(200).json({
      message: "Here are your tasks",
      data: tasks
    })
  }

  async store ({request, response}) {

    const taskData = request.only(['name', 'description, project_id'])
    const task = await task.create(taskData)

    response.status(201).json({
      message: "Successfully created a new task",
      data: task
    })
  }

  async show ({request, response,  params: {id}}) {
      const {task} = request.post()
      response.status(200).json({
        message: "Here is your task",
        data: task
      })

  }


  async update ({request, response,  params: {id}}) {

    const { task, name, description, project_id  } = request.post()

    task.merge({name, descripton, project_id})
    await task.save()

    response.status(201).json({
      message:"Successfully updated a new task",
      data: task
    })

  }

  async destroy ( {request, params: {id}}) {
    const { task} = request.post()
    task.delete()

    response.status(204).json({
      message:"Successfully deleted this task",
      deleted: true
    })

  }
}

module.exports = TaskController
