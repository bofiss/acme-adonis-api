'use strict'
const User = use('App/Models/User')

class UserController {
    async index({response}) {
        const user = await User.all();
        response.status('200').json({
            message: "All users here.",
            data: user
        })
    }

    async store({request, response}) {

        const { username, email, password} = request.post()
        const users = await User.create({ username, email, password})
        response.status('201').json({
            message: "Successfully created a new User",
            data: users
        })
    }  
    
    async show({request, response}) {

        const user= request.post().user

        response.status('201').json({
            message: "Here is the User",
            data: user
        })
    }

    async update({request, response, params: {id}}) {

        const user = request.post().user
    
        const { username, email, password } = request.post()
        user.merge({ username, email, password })
        await user.save()
         
         response.status('201').json({
             message: "Successfully updated User",
             data: user
         })
     }
}

module.exports = UserController

