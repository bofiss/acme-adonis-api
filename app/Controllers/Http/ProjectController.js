'use strict'

const Project = use('App/Models/Project')

class ProjectController {

  async index ({ response }) {
     
      const projects = await Project.all()
   
      response.status(200).json({
        message: "Here are your projects",
        data: projects
      })
  }

  async store ({request, response}) {
    const {name, description, customer_id, completed, tags} = request.post()
    const project = await Project.create({name, description, customer_id, completed})

    if(tags && tags.length > 0) {
      await project.tags().attach(tags)
      project.tags = await project.tags().fetch()
    }

    response.status(201).json({
      message: "Successfully created a new customer",
      data: project
    })
  }

  async show ({request, response, params: {id}}) {

    const {project} = request.post()

    response.status(200).json({
      message: "Here is your project",
      data: project
    })

  }


  async update ({request, response, params: {id}}) {
    const { project, name, description , customer_id, tags  } = request.post()

    project.merge({name , description, customer_id})
    await  project.save()

    if(tags && tags.length > 0) {
      await project.tags().detach()
      await project.tags().attach(tags)
      project.tags = await project.tags().fetch()
    }

    response.status(201).json({
      message:"Successfully updated a new project",
      data: project
    })

  }

  async destroy ({response, request, params: {id}}) {
    const { project} = request.post()
    project.delete()

    response.status(204).json({
      message:"Successfully deleted this project",
      deleted: true
    })

  }

}

module.exports = ProjectController
