'use strict'
const Customer = use('App/Models/Customer')

class CustomerController {

  async index ({response}) {
   
    const customers = await Customer.all() 
    response.status(200)
    .json({
      message: "Here are your customers",
      data: customers
    })
  }

  async store ({ request, response }) {

    const { name, description, user_id } = request.post()
    const customer = await Customer.create({name, description, user_id})

    response.status(201).json({
      message: "Successfully created a new customer",
      data: customer
    })
  }

  async show ({request, response}) {

     const { customer } = request.post()
     
     response.status(200).json({
          message: "Here is your customer",
          data: customer
      })
     
  }

  async update ({response, request}) {

      const { customer } = request.post()
      const { name, description, user_id } = request.post()

      customer.merge({name, description, user_id})
      await customer.save()

      response.status(201).json({
         message: "Successfully updated a new customer",
         data: customer
      })

  }

  async destroy ({request, response}) {
    const { customer } = request.post()
    customer.delete()
    response.status(204).json({
      message: "Successfully deleted this customer",
      deleted: true
    })
  }
}

module.exports = CustomerController
