'use strict'

class TagController {
  async index ({request, response}) {

    const tags = await Tag.all();
    response.status(200).json({
      message: "Here are your tags",
      data: tags
    })
  }

  async store ({request, response}) {

    const tagData = request.only(['name', 'description'])
    const Tag = await Tag.create(tagData)

    response.status(201).json({
      message: "Successfully created a new tag",
      data: Tag
    })
  }

  async show ({request, response,  params: {id}}) {
      const {tag} = request.post()
      response.status(200).json({
        message: "Here is your project",
        data: tag
      })

  }


  async update ({request, response,  params: {id}}) {
    const { tag, name, description  } = request.post()

    tag.merge({name, descripton})
    await  tag.save()

    response.status(201).json({
      message:"Successfully updated a new tag",
      data: tag
    })

  }

  async destroy ({request, response,  params: {id}}) {
    const { tag} = request.post()
    tag.delete()

    response.status(204).json({
      message:"Successfully deleted this project",
      deleted: true
    })

  }

}

module.exports = TagController
