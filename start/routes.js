'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

  Route.post('customers', 'CustomerController.store')
  Route.get('customers', 'CustomerController.index')
  Route.put('customers/:id', 'CustomerController.update').middleware(['findCustomer'])
  Route.delete('customers/:id', 'CustomerController.destroy').middleware(['findCustomer'])
  Route.get('customers/:id', 'CustomerController.show').middleware(['findCustomer'])


  Route.post('projects', 'ProjectController.store')
  Route.get('projects', 'ProjectController.index')
  Route.put('projects/:id', 'ProjectController.update').middleware(['findProject'])
  Route.delete('projects/:id', 'ProjectController.destroy').middleware(['findProject'])
  Route.get('projects/:id', 'ProjectController.show').middleware(['findProject'])


  Route.post('tags', 'TagController.store')
  Route.get('tags', 'TagController.index')
  Route.put('tags/:id', 'TagController.update').middleware(['findTag'])
  Route.delete('tags/:id', 'TagController.destroy').middleware(['findTag'])
  Route.get('tags/:id', 'TagController.show').middleware(['findTag'])

  Route.post('tasks', 'TaskController.store')
  Route.get('tasks', 'TaskController.index')
  Route.put('tasks/:id', 'TaskController.update').middleware(['findTask'])
  Route.delete('tasks/:id', 'TaskController.destroy').middleware(['findTask'])
  Route.get('tasks/:id', 'TaskController.show').middleware(['findTask'])


Route.post('users', 'UserController.store')
Route.get('users', 'UserController.index')